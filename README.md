# MoneyLion Assessment

Technical Assessment for MoneyLion job interview for Senior Software Engineer (Backend). This is a simple project where manager can enable or disable certain feature based on user's email and feature names. The manager can also check if certain feature is enabled or not for the combination of user's email and feature name.  

This project is written in **PHP** language using **MySQL** database.

## Description
There are two endpoints to perform above mentioned tasks.  

- **GET /feature?email=XXX&featureName=XXX**  
This endpoint receives an email which is used to check if that email has access to the featureName or not.  
The response is in JSON format as below:  
```json
{
    "canAccess": true|false (true if the email has access to the featureName otherwise false)
}

```  
- **POST /feature**  
This endpoint receives requests in JSON format as shown below.  
```json
{
    "featureName": "xxx", (string)
    "email": "xxx", (string)
    "enable": true|false (boolean) (true to enable access, otherwise false)
}

```  

This endpoint will first check for the following validations:  
1. Required fields: **featureName** and **email** are required fields. **enable** is optional and will be `false` by default.  
2. Correct email format.  
3. Combination of email and featureName should be unique.  

After validation is successful then it will insert the record into the database.  
This endpoint returns an empty response with **HTTPS Status OK (200)** if successfully inserted into the database otherwise it returns **HTTP Status Not Modified (304)**.

## Database Setup

Before using the above mentioned endpoints we first need to setup the database in our local machine. To do so please access `index.php` from the root folder. A database setup form will open where you will need to provide `database host`, `database username` and `database password`. After you submit the form, the required database `moneylion_assessment` and the required table `features` will be setup and a success message will be displayed.  

Then we are good to use the two endpoints.