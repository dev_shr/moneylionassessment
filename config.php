<?php
/**
 * array to store database configuration values
 */
    $database_config = array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'database' => 'moneylion_assessment',
        'table_feature' => 'features'
    );
?>