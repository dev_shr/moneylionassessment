<?php
/**
 * Database class
 * contains all the methods required for database operations
 */

class Database{

    protected $db_host;
    protected $db_username;
    protected $db_password;
    protected $db_database;
    protected $db_tablename;
    protected $db_connection;


    /**
     * the construct function
     * @param $db database configuration parameter passed when creating database object which inturn calls dbConnect to connect to database
     */
    function __construct($db){

        $this->db_host = $db['host'];
        $this->db_username = $db['username'];
        $this->db_password = $db['password'];
        $this->db_database = $db['database'];

        $this->dbConnect();
    }

    /**
     * function to connect to database
     * if any error connecting to database then will show the error message
     */
    private function dbConnect(){

        $this->db_connection = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_database);
        if($this->db_connection->connect_error){
            die("Connection error: ". $this->db_connection->connect_error);
        }
    }

    /**
     * function to set any table name for the database operation
     */
    function setTablename($table_name){

        $this->db_tablename = $table_name;
    }

    /**
     * function to insert requested data into database
     * @param array $insert_data array with key as field name and value as field value to be inserted into database
     */
    function insertRecord($insert_data){

        //prepare the field names from the key
        $insert_columns = implode(", ", array_keys($insert_data));

        //prepare the field values from the array value
        $escaped_values = array_map(array($this->db_connection, 'real_escape_string'), array_values($insert_data));

        //convert the field values into a string
        $insert_values = implode("', '", $escaped_values);

        //prepare the insert sql query
        $insert_sql = "INSERT INTO ".$this->db_tablename." ($insert_columns) VALUES ('$insert_values')";
        $insert = $this->db_connection->query($insert_sql);

        return $insert;
    }

    /**
     * function to check if provided email has access to provided feature name or not
     * @param array $conditions_array array for where condition
     */
    function getAccessRecord($conditions_array){

        //call getWhereConditions function to prepare where clause
        $where = $this->getWhereConditions($conditions_array);

        //prepare select query to check if provided email has access to provided feature name or not
        $get_sql = "SELECT * FROM ".$this->db_tablename.$where;
        $result = $this->db_connection->query($get_sql);

        //if there are any records then return the record otherwise return false
        if($result->num_rows > 0){
            $record = $result->fetch_assoc();
            return $record;
        }else{
            return false;
        }
    }

    /**
     * function to prepare where clause based on provided conditions
     * @param array $conditions array with key as field name and value as field value
     */
    private function getWhereConditions($conditions){
        //counter to count number of conditions added
        $counter = 1;

        //get how many conditions are there
        $conditions_size = sizeof($conditions);
        $where_condition = "";

        //if there are any conditions provided
        if($conditions_size > 0){
            $where_condition = " WHERE";

            //loop through each conditions
            foreach($conditions as $column=>$value){
                //prepare where clause as column = value
                $where_condition.= " $column = '$value'";

                //if there are still more conditions then add AND in the where clause
                if($conditions_size > $counter){
                    $where_condition.= " AND";

                    //increase the condition counter
                    $counter++;
                }
            }
        }

        return $where_condition;
    }

    /**
     * function to create database
     * @param String $db_name name of the database to be created
     * @return JSON if database is created then status will be true otherwise false and error message will be passed
     */
    function createDatabase($db_name){

        $create_db_sql = "CREATE DATABASE $db_name";
        if($this->db_connection->query($create_db_sql)){
            return json_encode(array('status' => true));
        }else{
            return json_encode(array('status' => false, 'error' => $this->db_connection->error));
        }
    }

    /**
     * function to create table
     * @param String $table_name name of the table to be created
     * @return JSON if table is created then status will be true otherwise false and error message will be passed
     */
    function createTable($table_name){

        $create_table_sql = "CREATE TABLE `$table_name` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(100) NOT NULL,
            `email` varchar(100) NOT NULL,
            `enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=>false, 1=>true',
            `created_at` timestamp NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        
        if($this->db_connection->query($create_table_sql)){
            return json_encode(array('status' => true));
        }else{
            return json_encode(array('status' => false, 'error' => $this->db_connection->error));
        }
    }

    /**
     * the destruct function
     * called at the end to close the db connection
     */
    function __destruct(){

        $this->db_connection->close();
    }
}

?>