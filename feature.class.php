<?php
/**
 * Feature class
 */

//include database.class.php to access all methods related to database operations
include "database.class.php";

class Feature{

    protected $feature_name;
    protected $email;
    protected $enable;
    protected $db;

    /**
     * the construct function
     * @param $db_config database configuration parameter. The database configuration parameter is passed to database class to connect to database
     */
    function __construct($db_config){
        $this->db = new Database($db_config);

        //call setTablename method to set the table name for the feature endpoints
        $this->db->setTablename($db_config['table_feature']);
    }

    /**
     * initial function for the endpoints which checks whether its POST or GET
     * @param $params for POST we get the $params and for GET we don't
     * @return $result of the endpoint
     */
    function init($params){

        //if got $params then its POST otherwise its GET
        if($params){
            //decode the json and convert it to array
            $data = json_decode($params, true);
            $this->feature_name = $data['featureName'];
            $this->email = $data['email'];
            $this->enable = $data['enable']?$data['enable']:0;

            //validation for the request params
            $result = $this->validate();
        }else{
            //get featureName and email
            $this->feature_name = $_GET['featureName'];
            $this->email = $_GET['email'];
            
            //perform the getAction to check the feature access
            $result = $this->getAction();
        }

        return $result;
    }

    /**
     * validate function to check the request params
     * checks if feature name or email is empty or not
     * checks the correct email format
     * checks if the combination of feature name and email already exists or not. Same feature cannot be assigned to same email address more than once
     * if validation is passed then call postAction method that will insert the request in database otherwise return the error message
     */
    private function validate(){

        if(empty($this->feature_name) || empty($this->email)){
            return (empty($this->feature_name)?'featureName':'email').' is required';
        }elseif(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            return 'email format is invalid';
        }else{
            $where_conditions = array('name' => $this->feature_name, 'email' => $this->email);
            $fetch_records = $this->db->getAccessRecord($where_conditions);

            //if the combination of feature name and email already exists
            if(is_array($fetch_records)){
                return 'The combination of featureName and email already exist.';
            }
        }

        $post_action = $this->postAction();
        return $post_action;
    }

    /**
     * function called for POST method endpoint
     * calls insertRecord in database class which inserts the request in database
     * @return Status Http status 200 if inserted into database. Http status 304 if got any issue
     */
    private function postAction(){

        //prepare an array with key as field name and value as field value to be inserted into database
        $insert_array = array('name' => $this->feature_name, 'email' => $this->email, 'enable' => $this->enable, 'created_at' => date("Y-m-d H:i:s"));
        $result = $this->db->insertRecord($insert_array);

        if($result){
            return http_response_code(200);
        }else{
            return http_response_code(304);
        }

    }

    /**
     * function called for GET method endpoint
     * calls getAccessRecord in database class which checks if provided email has access to provided feature name or not
     * @return JSON canAccess:true if provided email has access to provided feature name otherwise canAccess:false
     */
    private function getAction(){

        //prepare an array for where condition with key as field name and value as field value
        $where_conditions = array('name' => $this->feature_name, 'email' => $this->email);
        $get_access_record = $this->db->getAccessRecord($where_conditions);

        if($get_access_record){
            return json_encode(array('canAccess' => $get_access_record['enable']?true:false));
        }else{
            return json_encode(array('canAccess' => false));
        }

    }

}

?>