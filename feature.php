<?php
/**
 * Receives both POST and GET endpoints and handle accordingly
 */

//include config.php for database configuration
//include feature.class.php class to access the methods required for the endpoints
include "config.php";
include "feature.class.php";

//using php://input along with function file_get_contents to receive JSON data
$request = file_get_contents('php://input');

//create a feature object
$feature = new Feature($database_config);

//call the method init from feature class
$initialize = $feature->init($request);

//print the response
echo $initialize;
?>