<?php
/**
 * Option to create database and table before using the endpoints
 */

 //include config.php for database configuration
 //include database.class.php to access all methods related to database operations
include "config.php";
include "database.class.php";

$db_setup = false;

//if database setup form is submitted
if(isset($_POST['submit'])){

    //prepare database credentials received from the setup form
    $database_cred = array('host' => $_POST['host'], 'username' => $_POST['username'], 'password' => $_POST['password'], 'database' => '');

    //database credentials are passed to database class
    //notice database name is empty here as we need to create it first
    $db_connection = new Database($database_cred);

    //call createDatabase to create a database as defined in database config array
    $create_database_json = $db_connection->createDatabase($database_config['database']);
    $create_database = json_decode($create_database_json, true);

    //if any issue creating the database then display the error message
    if(!$create_database['status']){
        die($create_database['error']);
    }

    //update the database credentials array with correct database name now to be passed to database class and connect to it
    $db_connection = new Database(array_replace($database_cred, array('database' => $database_config['database'])));

    //call createTable to create a table as defined in database config array
    $create_table_json = $db_connection->createTable($database_config['table_feature']);
    $create_table = json_decode($create_table_json, true);

    //if any issue creating the table then display the error message
    if(!$create_table['status']){
        die($create_table['error']);
    }

    $db_setup = true;
}
?>

<div style="text-align: center">

<?php 
//if database setup is not done then display the setup form
//if database setup is done then display the success message
if(!$db_setup){?>
    
        <h2>Welcome to Database Setup</h2>
        <p>Please provide below information to create database</p>

        <form method="post">
            DB Host: <input type="text" id="host" name="host" required/></br></br>
            DB Username: <input type="text" id="username" name="username" required/></br></br>
            DB Password: <input type="password" id="password" name="password"/></br></br>
            <input type="submit" name="submit"/>
        </form>
<?php }else{ ?>
    <h1>Congratulations!!!</h2>
    <h2>Database Setup Completed.</h2>
    <p>Now you can use the endpoints. Please refer to README document.</p>
<?php } ?>
</div>